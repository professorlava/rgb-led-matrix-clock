#ifndef __CLOCK_RGBMAT88__
#define __CLOCK_RGBMAT88__

#include <avr/io.h>

#define NO_GHOSTING

/* Important Ports, see Schematic and ATmega specs*/
#define kLatchPort  PORTB
#define kLatchBit   PB2
#define kEnablePort PORTB
#define kEnableBit  PB1
#define kGroundPort PORTD
#define kClearPort  PORTB
#define kClearBit   PB0

/* Important Algorithm constants*/
#define kNumRows      8
#define kColorDepth   32
#define kRefreshRate  75
#define kClockDiv     64   /* Allows reasonably accurate CompVal from 256-16 depth*/
#ifdef NO_GHOSTING
    // Outer loop represents ROW frequency
    #define kIntFrequency (kNumRows * kRefreshRate)
#else
    // Outer loop represents PWM frequency
    #define kIntFrequency (kColorDepth * kRefreshRate)
#endif
#define kCounterRes   ((uint8_t)(F_CPU / kClockDiv / kIntFrequency) - 1)
#define kPwmStepMask  (kColorDepth - 1)
#define kMatrixSize 192

#define MATRIX_BUFFER_DRAW __vector_14

#define DEFINE_FRAME_BUFFER(x) uint8_t x[kMatrixSize] = {0}

extern volatile uint8_t g_pwm_step;
extern volatile uint8_t g_frame_buffer[kMatrixSize];

/******************
  Public Prototypes
 ******************/

/* Initialize Ports and Modules relevant to the Display
 * Ports Used:
 *     PORTB [1-5]
 * Modules Used:
 *     TIMER0
 *     SPI MODULE
 */
void init_rgbmat88 (void);

/* This is where the magic happens!
 * This function is gets linked into the ISR vector table and is called
 * whenever TIMER0_COMPA interrupt is triggered.
 * This interrupt generates the SPI communication which drives the external
 * shift registers and darlington array which controls the LED Matrix.
 */
void MATRIX_BUFFER_DRAW (void) __attribute__ ((signal,used,externally_visible));

/* Set a led in buffer to (red, green, blue)
 * Parameters:
 *	uint8_t *buffer: Buffer to manipulate, size of kMatrixSize
 *	uint8_t red,   : Color value to set, since kColorDepth is likely less than UINT8_MAX
 *              green, ^ so if the value is more than kColorDepth behavior is undefined (in
 *              blue,  ^ reality anything above kColorDepth will just equal kColorDepth)
 */
void set_depth_led (uint8_t * buffer,
                    uint8_t const led,
                    uint8_t const red, uint8_t const green, uint8_t const blue);
/* Add to a led in buffer  (red, green, blue)
 * Parameters:
 *	uint8_t *buffer: Buffer to manipulate, size of kMatrixSize
 *	uint8_t red,   : Color value to set, since kColorDepth is likely less than UINT8_MAX
 *              green, ^ so if the value is more than kColorDepth behavior is undefined (in
 *              blue,  ^ reality anything above kColorDepth will just equal kColorDepth)
 */
void add_depth_led (uint8_t * buffer,
                    uint8_t const led,
                    uint8_t const red, uint8_t const green, uint8_t const blue);

/* Set a led in buffer to (R, G, B)
 * input 0-255 is mapped to 0-kColorDepth
 * Parameters:
 *	uint8_t *buffer: Buffer to manipulate, size of kMatrixSize
 *	uint8_t red,   : Color value to set, 0-255 of uint8_t mapped to 0-kColorDepth
 *              green, ^
 *              blue,  ^
 */
void set_rgb_led (uint8_t * buffer,
                  uint8_t const led,
                  uint8_t const red, uint8_t const green, uint8_t const blue);

/* Add color a led in buffer to (R, G, B)
 * input 0-255 is mapped to 0-kColorDepth
 * Parameters:
 *	uint8_t *buffer: Buffer to manipulate, size of kMatrixSize
 *	uint8_t red,   : Color value to set, 0-255 of uint8_t mapped to 0-kColorDepth
 *              green, ^
 *              blue,  ^
 */
void add_rgb_led (uint8_t * buffer,
                  uint8_t const led,
                  uint8_t const red, uint8_t const green, uint8_t const blue);

/* Set a pixel in buffer to color depth
 * Parameters:
 *	uint8_t *buffer: Buffer to manipulate, size of kMatrixSize
 *	uint8_t red,   : Color value to set, since kColorDepth is likely less than UINT8_MAX
 *              green, ^ so if the value is more than kColorDepth behavior is undefined (in
 *              blue,  ^ reality anything above kColorDepth will just equal kColorDepth)
 */
void set_depth_pixel (uint8_t * buffer,
                      uint8_t const col, uint8_t const row,
                      uint8_t const red, uint8_t const green, uint8_t const blue);

/* Set a pixel in buffer to color depth
 * Parameters:
 *	uint8_t *buffer: Buffer to manipulate, size of kMatrixSize
 *	uint8_t red,   : Color value to set, since kColorDepth is likely less than UINT8_MAX
 *              green, ^ so if the value is more than kColorDepth behavior is undefined (in
 *              blue,  ^ reality anything above kColorDepth will just equal kColorDepth)
 */
void add_depth_pixel (uint8_t * buffer,
                      uint8_t const col, uint8_t const row,
                      uint8_t const red, uint8_t const green, uint8_t const blue);

/* Set a pixel in buffer to (R, G, B)
 * input 0-255 is mapped to 0-kColorDepth
 * Parameters:
 *	uint8_t *buffer: Buffer to manipulate, size of kMatrixSize
 *	uint8_t red,   : Color value to set, 0-255 of uint8_t mapped to 0-kColorDepth
 *              green, ^
 *              blue,  ^
 */
void set_rgb_pixel (uint8_t * buffer,
                uint8_t const col, uint8_t const row,
                uint8_t const red, uint8_t const green, uint8_t const blue);

/* Add to a pixel in buffer to (R, G, B)
 * input 0-255 is mapped to 0-kColorDepth
 * Parameters:
 *	uint8_t *buffer: Buffer to manipulate, size of kMatrixSize
 *	uint8_t red,   : Color value to set, 0-255 of uint8_t mapped to 0-kColorDepth
 *              green, ^
 *              blue,  ^
 */
void add_rgb_pixel (uint8_t * buffer,
                    uint8_t const col, uint8_t const row,
                    uint8_t const red, uint8_t const green, uint8_t const blue);

/* Copy contents of frame to the global buffer.
 * This call temporarily disables global interrupts to make copying quicker and safer.
 * TODO: Actually calculate time to complete
 * Parameters:
 *    uint8_t const* frame: frame buffer to copy into display buffer
 */
void buffer_flip (uint8_t const* frame);

/* Clears the global buffer of data.
 * This call temporarily disables global interrupts to make copying quicker and safer.
 * TODO: Actually calculate time to complete
 */
void buffer_clear (void);
void frame_clear (uint8_t * frame);

/* Copies the contents of the global buffer into the input buffer
 * This call temporarily disables global interrupts to make copying quicker and safer.
 * TODO: okay we get it, calculate it already
 */
void buffer_copy (uint8_t * frame);

/**************
  Inlined in .c
 **************/
//void spi_byte_when_ready(uint8_t payload);
//void consume_register(uint8_t count, uint8_t * buf_ref);
//#define pwm_cmp_add(sendbyte, counter, ptr) ;

#endif
