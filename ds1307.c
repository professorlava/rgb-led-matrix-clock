#include <avr/io.h>
#include <util/twi.h>

#include "ds1307.h"

#define DS1307 0xD0
#define INIT_REG 0x00

/* Data register number correlation */
#define HALT 0
#define SECONDS 0
#define MINUTES 1
#define HR12 2
#define AMPM 2
#define HOURS 2
#define DAY 3
#define DATE 4
#define MONTH 5
#define YEAR 6
#define CONTROL 7
#define NUM_REG 8

#define H_SHIFT 4

/* (0x00) Register 0 Masks*/
#define HALT_MSK      0b10000000
#define SECONDS_H_MSK 0b01110000
#define SECONDS_L_MSK 0b00001111
/* (0x01) Register 1 Masks*/
#define MINUTES_H_MSK 0b01110000
#define MINUTES_L_MSK 0b00001111
/* (0x02) Register 2 Masks*/
#define HR12_MSK      0b01000000
#define AMPM_MSK      0b00100000
#define HOURS_24H_MSK 0b00110000
#define HOURS_12H_MSK 0b00010000
#define HOURS_L_MSK   0b00001111
/* (0x03) Register 3 Masks*/
#define DAY_MSK       0b00000111
/* (0x04) Register 4 Masks*/
#define DATE_H_MSK    0b00110000
#define DATE_L_MSK    0b00001111
/* (0x05) Register 5 Masks*/
#define MONTH_H_MSK   0b00010000
#define MONTH_L_MSK   0b00001111
/* (0x06) Register 6 Masks*/
#define YEAR_H_MSK    0b11110000
#define YEAR_L_MSK    0b00001111
/* (0x07) Register 7 Masks*/
#define OUT_MSK       0b10000000
#define SQWE_MSK      0b00010000
#define RATE_MSK      0b00000011


#define UPPER_DECIMAL(REG,H_MSK) \
    ((uint8_t)((uint8_t)((g_rtc_bcd[REG] & H_MSK) >> H_SHIFT) * 10 ))
#define LOWER_DECIMAL(REG,L_MSK) \
    ((uint8_t)(g_rtc_bcd[REG] & L_MSK))
#define DECIMAL(REG,H_MSK,L_MSK) \
    ((uint8_t)(UPPER_DECIMAL(REG,H_MSK) + LOWER_DECIMAL(REG,L_MSK)))

#define BCD(VAR) \
    (((uint8_t)(VAR / 10U) << H_SHIFT) | ((uint8_t)(VAR % 10U)))

uint8_t g_rtc_bcd[NUM_REG] = {0};

void init_ds1307 (void) {
    //set SCL to 100kHz
    TWSR = 0x02;
    TWBR = 0x02;
    //init global data
    ds1307_read();
}

void twi_start (void) {
    TWCR = (1 << TWINT) | (1 << TWSTA) | (1 << TWEN);
    while( !(TWCR & (1 << TWINT)));
}

void twi_stop (void) {
    TWCR = (1 << TWINT) | (1 << TWSTO) | (1 << TWEN);
}

void twi_send (uint8_t data) {
    TWDR = data;
    TWCR = (1 << TWINT) | (1 << TWEN);
    while (!(TWCR & (1 << TWINT)));
}

uint8_t twi_recv_nack (void) {
    TWCR = (1 << TWINT) | (1 << TWEN);
    while (!(TWCR & (1 << TWINT)));
    return TWDR;
}

uint8_t twi_recv_ack (void) {
    TWCR = (1 << TWEA) | (1 << TWINT) | (1 << TWEN);
    while (!(TWCR & (1 << TWINT)));
    return TWDR;
}

void ds1307_write (void) {
    TWCR = 0;
    twi_start();
    //if (TW_STATUS != TW_START)
    //    return TW_STATUS;

    // Enter Master Transmit mode
    twi_send(DS1307 | TW_WRITE);
    //if (TW_STATUS != TW_MT_SLA_ACK) {
    //    twi_stop();
    //    return TW_STATUS;
    //}

    // Set RTC reg ptr to initial register
    twi_send(INIT_REG);
    //if (TW_STATUS != TW_MT_DATA_ACK) {
    //    twi_stop();
    //    return TW_STATUS;
    //}

    // Set all 7 registers from data
    for (uint8_t reg = SECONDS; reg < NUM_REG; ++reg) {
        twi_send(g_rtc_bcd[reg]);
        //if (TW_STATUS != TW_MT_DATA_ACK) {
        //    twi_stop();
        //    return TW_STATUS;
        //}
    }

    twi_stop();
    //return TW_STATUS;
}

void ds1307_read (void) {
    TWCR = 0;
    twi_start();
    //if (TW_STATUS != TW_START)
    //    return TW_STATUS;

    // Enter Master Transmit mode
    twi_send(DS1307 | TW_WRITE);
    //if (TW_STATUS != TW_MT_SLA_ACK) {
    //    twi_stop();
    //    return TW_STATUS;
    //}

    // Set RTC reg ptr to initial register
    twi_send(INIT_REG);
    //if (TW_STATUS != TW_MT_DATA_ACK) {
    //    twi_stop();
    //    return TW_STATUS;
    //}

    // Repeat start for reading
    twi_start();
    //if (TW_STATUS != TW_START)
    //    return TW_STATUS;

    // Enter Master Receive mode
    twi_send(DS1307 | TW_READ);
    //if (TW_STATUS != TW_MR_SLA_ACK) {
    //    twi_stop();
    //    return TW_STATUS;
    //}

    // Recv the data! yeay
    for (uint8_t reg = SECONDS; reg < CONTROL; ++reg) {
        g_rtc_bcd[reg] = twi_recv_ack();
        //if (TW_STATUS != TW_MR_DATA_ACK) {
        //    twi_stop();
        //    return TW_STATUS;
        //}
    }
    g_rtc_bcd[CONTROL] = twi_recv_nack();

    twi_stop();
    //return TW_STATUS;
}

uint8_t ds1307_halted (void) {
    return (g_rtc_bcd[HALT] & HALT_MSK);
}

void ds1307_set_halt (uint8_t val) {
    if (val) {
        g_rtc_bcd[HALT] |= HALT_MSK;
    } else {
        g_rtc_bcd[HALT] &= ~HALT_MSK;
    }
}

uint8_t ds1307_out (void) {
    return (g_rtc_bcd[CONTROL] & OUT_MSK);
}

void ds1307_set_out (uint8_t out) {
    if (out) {
        g_rtc_bcd[CONTROL] |= OUT_MSK;
    } else {
        g_rtc_bcd[CONTROL] &= ~OUT_MSK;
    }
}

void ds1307_get_sqw(uint8_t * sqwe, uint8_t * rate) {
    * sqwe = (g_rtc_bcd[CONTROL] & SQWE_MSK);
    * rate = (g_rtc_bcd[CONTROL] & RATE_MSK);
}

void ds1307_set_sqw(uint8_t sqwe, uint8_t rate) {
    if (sqwe) {
        g_rtc_bcd[CONTROL] |= (SQWE_MSK | (rate & RATE_MSK));
    } else {
        g_rtc_bcd[CONTROL] &= ~SQWE_MSK;
    }
}

void ds1307_get_seconds (uint8_t * sec) {
    *sec = DECIMAL(SECONDS,SECONDS_H_MSK,SECONDS_L_MSK);
}

void ds1307_set_seconds (uint8_t sec) {
    g_rtc_bcd[SECONDS] = (g_rtc_bcd[SECONDS] & HALT_MSK) | BCD(sec) ;
}

void ds1307_get_minutes (uint8_t * min) {
    *min = DECIMAL(MINUTES,MINUTES_H_MSK,MINUTES_L_MSK);
}

void ds1307_set_minutes (uint8_t min) {
    g_rtc_bcd[MINUTES] = BCD(min);
}

void ds1307_get_hours (uint8_t * hour, uint8_t * flags) {
    uint8_t hr12 = (g_rtc_bcd[HR12] & HR12_MSK);
    uint8_t ampm = (g_rtc_bcd[AMPM] & AMPM_MSK);
    if (hr12) {
        *hour = DECIMAL(HOURS,HOURS_12H_MSK,HOURS_L_MSK);
    } else {
        *hour = DECIMAL(HOURS,HOURS_24H_MSK,HOURS_L_MSK);
    }
    *flags = (hr12 << 6) | (ampm << 5);
}

void ds1307_set_hours (uint8_t hour, uint8_t flags) {
    g_rtc_bcd[HOURS] = (uint8_t)(flags << H_SHIFT) | BCD(hour);
}

void ds1307_get_day (uint8_t * day) {
    *day = (g_rtc_bcd[DAY] & DAY_MSK);
}

void ds1307_set_day (uint8_t day) {
    g_rtc_bcd[DAY] = day;
}

void ds1307_get_date (uint8_t * date) {
    *date = DECIMAL(DATE,DATE_H_MSK,DATE_L_MSK);
}

void ds1307_set_date (uint8_t date) {
    g_rtc_bcd[DATE] = BCD(date);
}

void ds1307_get_month (uint8_t * month) {
    *month = DECIMAL(MONTH,MONTH_H_MSK,MONTH_L_MSK);
}
void ds1307_set_month (uint8_t month) {
    g_rtc_bcd[DATE] = BCD(month);
}

void ds1307_get_year (uint8_t * year) {
    *year = DECIMAL(YEAR,YEAR_H_MSK,YEAR_L_MSK);
}
void ds1307_set_year (uint8_t year) {
    g_rtc_bcd[YEAR] = BCD(year);
}
