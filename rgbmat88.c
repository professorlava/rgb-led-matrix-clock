#include <avr/io.h>
#include <string.h>

#ifndef F_CPU
#   define F_CPU 8000000UL
#endif

#include "rgbmat88.h"

/******************
 Private Prototypes
 ******************/

/* Send a single byte of data through the SPI device, as soon as it is available
 * but don't block after we set the data; This is to allow the gathering of the
 * next payload while the SPI device does its work.
 */
void spi_byte_when_ready(uint8_t payload);

/* Calling pwm_cmp_add 8 times means payload's bits are set based
 * on the pwm count and buf_ptr[i] through buf_ptr[i-8].
 * WARNING: Despite the pointer being passed rather than a pointer to a pointer
 * 	    it IS MODIFIED by this call, hence the use of consume in the fcn name
 * After payload is created from pwm data, it is sent to the SPI module
 */
void consume_register(uint8_t count, uint8_t * buf_ref);

/* Nice macro using only 3 instructions per pin to generate the byte to transfer
 * Retreive duty cycle setting from memory and dec. ptr (ld, 2 cycles)
 * Compare with the counter (cp, 1 cycle) --> result is stored in carry
 * Use the rotate over carry right to shift the compare result into the byte. (1 cycle).
 */
//XXX Change ROR to ROL to fix X axis mirror
#define pwm_cmp_add(sendbyte, counter, ptr) {       \
    __asm__ __volatile__ (                          \
        "ld __tmp_reg__, - %a1" "\n\t"              \
        "cp %2, __tmp_reg__"    "\n\t"              \
        "ror %0"                "\n\t"              \
        : "=r" (sendbyte)                           \
        : "e" (ptr), "r" (counter), "0" (sendbyte)  \
        : "memory"                                  \
    );                                              \
}

/*************
 Globals! BOO oh well
 *************/
volatile uint8_t g_isr_step = 0;
volatile uint8_t g_matrix_buffer[kMatrixSize] = {0};

/*************************
 Public API Implementation
 *************************/

void buffer_flip (uint8_t const* frame) {
    asm volatile("cli" ::: "memory");
    memcpy((void *)g_matrix_buffer, (void *)frame, kMatrixSize);
    asm volatile("sei" ::: "memory");
}

void frame_clear( uint8_t * frame) {
    memset((void *)frame, 0, kMatrixSize);
}

void buffer_clear (void) {
    asm volatile("cli" ::: "memory");
    memset((void *)g_matrix_buffer, 0, kMatrixSize);
    asm volatile("sei" ::: "memory");
}

void buffer_copy (uint8_t * frame) {
    asm volatile("cli" ::: "memory");
    memcpy((void *)frame, (void *)g_matrix_buffer, kMatrixSize);
    asm volatile("sei" ::: "memory");
}

void set_depth_pixel (uint8_t * frame,
                      uint8_t const col, uint8_t const row,
                      uint8_t const red, uint8_t const green, uint8_t const blue) {
    uint8_t const index = row * 24 + col;
    frame[index] = red;
    frame[index + 8] = green;
    frame[index + 16] = blue;
}

void add_depth_pixel (uint8_t * frame,
                      uint8_t const col, uint8_t const row,
                      uint8_t const red, uint8_t const green, uint8_t const blue) {
    uint8_t const index = row * 24 + col;
    frame[index] += red;
    frame[index + 8] += green;
    frame[index + 16] += blue;
}

void set_rgb_pixel (uint8_t * frame,
                    uint8_t const col, uint8_t const row,
                    uint8_t const red, uint8_t const green, uint8_t const blue) {
    uint8_t const index = row * 24 + col;
    frame[index] = (uint8_t)((red * kColorDepth) >> 8);
    frame[index + 8] = (uint8_t)((green * kColorDepth) >> 8);
    frame[index + 16] = (uint8_t)((blue * kColorDepth) >> 8);
}

void add_rgb_pixel (uint8_t * frame,
                    uint8_t const col, uint8_t const row,
                    uint8_t const red, uint8_t const green, uint8_t const blue) {
    uint8_t const index = row * 24 + col;
    frame[index] += (uint8_t)((red * kColorDepth) >> 8);
    frame[index + 8] += (uint8_t)((green * kColorDepth) >> 8);
    frame[index + 16] += (uint8_t)((blue * kColorDepth) >> 8);
}

void set_depth_led (uint8_t * frame,
                    uint8_t const led,
                    uint8_t const red, uint8_t const green, uint8_t const blue) {
    uint8_t const index = ((uint8_t)((led >> 3) << 4) + led);
    frame[index] = red;
    frame[index + 8] = green;
    frame[index + 16] = blue;
}

void add_depth_led (uint8_t * frame,
                    uint8_t const led,
                    uint8_t const red, uint8_t const green, uint8_t const blue) {
    uint8_t const index = ((uint8_t)((led >> 3) << 4) + led);
    frame[index] += red;
    frame[index + 8] += green;
    frame[index + 16] += blue;
}

void set_rgb_led (uint8_t * frame,
                  uint8_t const led,
                  uint8_t const red, uint8_t const green, uint8_t const blue) {
    uint8_t const index = ((uint8_t)((led >> 3) << 4) + led);
    frame[index] = (uint8_t)((red * kColorDepth) >> 8);
    frame[index + 8] = (uint8_t)((green * kColorDepth) >> 8);
    frame[index + 16] = (uint8_t)((blue * kColorDepth) >> 8);
}

void add_rgb_led (uint8_t * frame,
                  uint8_t const led,
                  uint8_t const red, uint8_t const green, uint8_t const blue) {
    uint8_t const index = ((uint8_t)((led >> 3) << 4) + led);
    frame[index] += (uint8_t)((red * kColorDepth) >> 8);
    frame[index + 8] += (uint8_t)((green * kColorDepth) >> 8);
    frame[index + 16] += (uint8_t)((blue * kColorDepth) >> 8);
}

void init_rgbmat88 (void) {
    /* Pin Configuration */
    DDRB |= (1 << DDB5) | (1 << DDB3) | (1 << kLatchBit)
         | (1 << kEnableBit) | (1 << kClearBit);
    DDRD |= 0xFF;

    /* Timer/Counter0 */
    TCCR0A |= (1 << WGM01); // CTC mode
    TCCR0B |= (1 << CS01) | (1 << CS00);//0 1 1 clkIO/64 (From prescaler)
    OCR0A = kCounterRes;
    TIMSK0 |= (1 << OCIE0A); // enable COMPA interupt

    /* SPI setup */
    // SPI2X SPR1 SPR0
    //   1     0     0    fosc/2
    SPCR |= (1 << SPE) | (1 << MSTR);
    SPCR &= ~((1 << SPR1) | (1 << SPR0));
    SPSR |= (1 << SPI2X);
}

inline void spi_byte_when_ready (uint8_t payload) {
    // Block till interupt flag signals completion
    while (!(SPSR & (uint8_t)(1 << SPIF))){};
    SPDR = payload;
}

inline void consume_register(uint8_t const count, uint8_t * buf_ptr) {
    uint8_t payload = 0;

    pwm_cmp_add(payload, count, buf_ptr);
    pwm_cmp_add(payload, count, buf_ptr);
    pwm_cmp_add(payload, count, buf_ptr);
    pwm_cmp_add(payload, count, buf_ptr);
    pwm_cmp_add(payload, count, buf_ptr);
    pwm_cmp_add(payload, count, buf_ptr);
    pwm_cmp_add(payload, count, buf_ptr);
    pwm_cmp_add(payload, count, buf_ptr);

    spi_byte_when_ready(payload);
}

#ifdef NO_GHOSTING

/* This algorithm is Row Major, mutliplexing the rows with the lowest frequency (outer most "loop")
 * while the "inner loop" iterates through the pwm steps.
 * Each call of this function draws a single row with pwm at the current frame, and is called based on
 * the number of rows and the desired refresh rate.
 *
 * General Gotcha: Both algorithms do not display their last row of data until the next interupt
 *
 * This algorithm has NO GHOSTING
 */
void MATRIX_BUFFER_DRAW (void) {
    uint8_t row = g_isr_step;
    uint8_t * buf_ptr = (uint8_t *) g_matrix_buffer + (1 + row) * 24;
    uint8_t count = 0;

    // Kickstart SPI
    SPDR = 0;

    while(count < kPwmStepMask) {
        // Enable Display; LAST ITERATIONS data!
        kEnablePort &= (uint8_t)~(1 << kEnableBit);

        // Blue data represents the last 8 bytes, first to be sent
        consume_register(count, buf_ptr);
        // Green data represents the middle 8 bytes
        consume_register(count, buf_ptr);
        // Red data represents the first 8 bytes of a row
        consume_register(count, buf_ptr);

        count++;
        buf_ptr += 24;

        // Disable display Momentarily
        kEnablePort |= (uint8_t)(1 << kEnableBit);

        // Ground Row
        kGroundPort = (1 << row);

        // Is SPI done yet?
        while (!(SPSR & (uint8_t)(1 << SPIF)));

        // Latch data into storage register, by toggling RCK
        kLatchPort |= (uint8_t)(1 << kLatchBit);
        kLatchPort &= (uint8_t)~(1 << kLatchBit);
    }

    g_isr_step = (uint8_t)((++row & 0x7));
}

#else

/* This algorithm is likely faster of the two.
 * PWM Major, meaning the "outer loop" is the PWM modulation, and the actual function
 * (inner loop) iterates through rows.
 *
 * General Gotcha: Both algorithms do not display their last row of data until the next interupt
 *
 * This algorithm may have ghosting?
 *
 * XXX CURRENTLY BROKEN, doesnt work right with -O3
 */
void MATRIX_BUFFER_DRAW (void) {
    uint8_t * buf_ptr = (uint8_t *) g_matrix_buffer + kMatrixSize;
    uint8_t count = g_isr_step;
    uint8_t row = (1 << 7);

    // Kickstart SPI
    SPDR = 0;

    while(row) {
	// Enable Display; LAST ITERATIONS data!
    	kEnablePort &= (uint8_t)~(1 << kEnableBit);

        // Blue data represents the last 8 bytes, first to be sent
        consume_register(count, buf_ptr);
        // Green data represents the middle 8 bytes
        consume_register(count, buf_ptr);
        // Red data represents the first 8 bytes of a row
        consume_register(count, buf_ptr);

	// Is SPI done yet?
        while (!(SPSR & (uint8_t)(1 << SPIF)));

        // Disable display Momentarily
        kEnablePort |= (uint8_t)(1 << kEnableBit);

        // Ground Row
        kGroundPort = row;
        row >>= 1;

        // Latch data into storage register, by toggling RCK
        kLatchPort |= (uint8_t)(1 << kLatchBit);
        kLatchPort &= (uint8_t)~(1 << kLatchBit);
    }

    g_isr_step = (uint8_t)((++count) & kPwmStepMask);
}

#endif
