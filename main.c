#include <avr/io.h>

#ifndef F_CPU
#   define F_CPU 8000000UL
#endif

#include <util/delay.h>

#include "rgbmat88.h"
#include "ds1307.h"

/* Create a play(..) fcn which loads and plays data from eeprom */
void play_color (uint8_t, uint8_t, uint8_t);
void play_test (void);
void rtc_set_menu (void);
void rtc_reset(void);
void display_time (void);

int main (void) {
    asm volatile("cli" ::: "memory");
    /* Power off unused modules*/
    PRR |= (1 << PRTIM1) | (1 << PRUSART0) | (1 << PRADC);
    /* Initialize linked modules*/
    init_ds1307();
    init_rgbmat88();
    asm volatile("sei" ::: "memory");

    //INSERT:  Play Intro

    if (ds1307_halted()) {
        /* If RTC is halted when we start, this is the first time it is
         * turning on since the last time its backup battery was changed so
         * Reset */
        rtc_reset();
        //rtc_set_menu();
        ds1307_set_seconds(55);
        ds1307_set_minutes(59);
        ds1307_set_hours(3, 0);// DS1307_PM);
        ds1307_set_day(DS1307_SAT);
        ds1307_set_date(22);
        ds1307_set_month(4);
        ds1307_set_year(17);
        ds1307_write();
    }

    DEFINE_FRAME_BUFFER(fb);
    set_depth_pixel(fb,0,0,32,0,0);
    set_depth_pixel(fb,7,0,0,32,0);
    set_depth_pixel(fb,7,7,0,0,32);
    set_depth_pixel(fb,0,7,32,32,32);
    buffer_flip(fb);

    while (1) {
        // Use timer 2 and sqw from RTC to power events
        // Check for special events, and act upon them
        //      Display closed loop for event
        // otherwise display normal time
        //      Based on mode
        //play_test();
	//play_color(0,32,0);
        display_time();
    }
}

void play_color (uint8_t r, uint8_t g, uint8_t b) {
    DEFINE_FRAME_BUFFER(frame);
    for (uint8_t led = 0; led < 64; ++led) {
	frame_clear(frame);
    	set_depth_led (frame, led, r, g, b);
	buffer_flip(frame);
        _delay_ms(50);
    }
    buffer_clear();
}

/* If RTC is halted when we start, this is the first time it is
 * turning on since the last time its backup battery was changed so
 * make sure all data is initialized and start unhalt
 */

void rtc_reset (void) {
    ds1307_set_halt(0);
    ds1307_set_out(0);
    ds1307_set_sqw(0, 0);

    ds1307_write();
}

void rtc_set_menu (void) {
    // INSERT: Set Menu Logic
    ds1307_set_seconds(0);
    ds1307_set_minutes(0);
    ds1307_set_hours(0,0x0);
    ds1307_set_day(0);
    ds1307_set_date(0);
    ds1307_set_month(0);
    ds1307_set_year(0);

    ds1307_write();
}

uint8_t sec_old = 0;
uint8_t sec_track_i = 0;
uint8_t sec_track[28] = { 4, 3, 2, 1, 0, 8, 16, 24, 32,40, 48, 56, 57, 58, 59, 60, 61, 62, 63, 55, 47, 39, 31, 23, 15, 7, 6, 5 };

uint8_t min_track[20] = { 11, 10, 9, 17, 25, 33, 41, 49, 50, 51, 52, 53, 54, 46, 38, 30, 22, 14, 13, 12 };

uint8_t hour_track[12] = { 19, 18, 26, 34, 42, 43, 44, 45, 37, 29, 21, 20 };

void display_time (void) {
    DEFINE_FRAME_BUFFER(frame2);
    uint8_t secs, mins, hrs = 0, flags, r, g, b;

    ds1307_read();

    /* Display Seconds as wheel around display,
       blip alt color when 60 is hit*/
    ds1307_get_seconds(&secs);
    if (secs != sec_old) {
	sec_track_i += 1;
 	if ( sec_track_i >= 28) {
	    sec_track_i = 0;
        }
        sec_old = secs;
    }
    g = (secs/2 - 20);
    if (g > 32)
        g = 0;
    b = 8 + secs/2;
    r = 0;
    set_depth_led (frame2, sec_track[sec_track_i], r, g, b);

    /* Minutes */
    ds1307_get_minutes(&mins);
    b = 0;
    r = 24;
    for (int8_t m = 20 * (mins/20); m <= mins; ++m) {
        g = 6 * (m/20);
        set_depth_led (frame2, min_track[m % 20], 24, g, 0);
    }

    /* Hours */
    ds1307_get_hours(&hrs, &flags);
    for (int8_t h = 0; h < hrs; ++h) {
        set_depth_led (frame2, hour_track[h % 12], 0, 24, 0);
    }

    buffer_flip(frame2);
}

void play_test(void)
{
    DEFINE_FRAME_BUFFER(frame3);
    uint8_t r, g, b;

    for (uint8_t hours = 0; hours < 12; ++hours) {
        for (uint8_t mins = 0; mins < 60; ++mins) {
            for (uint8_t secs = 0; secs < 60; ++secs) {
                frame_clear(frame3);
                if (secs != sec_old) {
                    sec_track_i += 1;
                    if ( sec_track_i >= 28) {
                        sec_track_i = 0;
                    }
                    sec_old = secs;
                }
                g = (secs/2 - 15);
                if (g > 32)
                    g = 0;
                b = 8 + secs/2;
                r = 0;
                set_depth_led (frame3, sec_track[sec_track_i], r, g, b);

	        for (int8_t m = 20 * (mins/20); m <= mins; ++m) {
        	    g = 6 * (m/20);
	            set_depth_led (frame3, min_track[m % 20], 24, g, 0);
        	}

                for (int8_t h = 0; h < hours; ++h) {
		        set_depth_led (frame3, hour_track[h % 12], 16, 0, 16);
        	}

                buffer_flip(frame3);
                _delay_ms(5);
            }
        }
    }
}
