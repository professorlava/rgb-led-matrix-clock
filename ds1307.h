#ifndef __CLOCK_DS1307__
#define __CLOCK_DS1307__

/* Initialize IO and state for this module
 * After calling, all other functions in this
 * module will opperate as described
 */
void init_ds1307 (void);

/* Read in state of DS1307 RTC IC.
 * I2C protocol goes as follows:
 *  Send START,
 *  Send DS1307_ADDR + WRITE, Recv Ack,
 *  Send INIT_REG_ADDR, Recv Ack,
 *  Send RESTART,
 *  Send DS1307_ADDR + READ, Recv Ack,
 *  for i=1..6
 *    Recv DATA, Send Ack
 *  Recv DATA, Send Nack
 *  Send STOP
 */
void ds1307_read  (void);

/* Write state rtc data to DS1307 RTC IC.
 * I2C protocol goes as follows:
 *  Send START,
 *  Send DS1307_ADDR + WRITE, Recv Ack,
 *  Send INIT_REG_ADDR, Recv Ack,
 *  for i=1..7
 *    Send DATA, Recv Ack
 *  Send STOP
 */
void ds1307_write (void);

/***********
 ds1307_read should be called at least once before a round of getters
 in order to get accurate data.
 ds1307_write should be called after a round of setters in order to
 actually write the data to the RTC
 ***********/

/* The clock is halted in the its initial factor state, when powering on after having
 * NOT been maintained by a backup battery.
 * It must be unhalted to begin counting. Unhalting while unhaulted is not harmful
 */
#define DS1307_HALT_OFF 0
#define DS1307_HALT_ON 1
uint8_t ds1307_halted (void);
void ds1307_set_halt (uint8_t val);

/* Output Control (OUT). This bit controls the output level of the SQW/OUT
 * pin when the square-wave output is disabled. If SQWE = 0, the logic
 * level on the SQW/OUT pin is 1 if OUT = 1 and is 0 if OUT = 0. On initial
 * application of power to the device, this bit is typically set to a 0.
 */
uint8_t ds1307_out (void);
void ds1307_set_out (uint8_t val);

/* This bit, when set to logic 1, enables the oscillator output. The frequency
 * of the square-wave output depends upon the value of the RS0 and RS1 bits.
 * With the square-wave output set to 1Hz, the clock registers update on the
 * falling edge of the square wave. On initial application of power to the
 * evice, this bit is typically set to a 0.
 */
#define DS1307_SQW_1Hz   0
#define DS1307_SQW_4kHz  1//4.096 kHz
#define DS1307_SQW_8kHz  2//8.192 kHz
#define DS1307_SQW_32kHz 3//32.768 kHz
void ds1307_set_sqw(uint8_t sqwe, uint8_t rate);
void ds1307_get_sqw(uint8_t * sqwe, uint8_t * rate);

/* Get and set seconds data
 * range: 00-59
 */
void ds1307_get_seconds (uint8_t * sec);
void ds1307_set_seconds (uint8_t sec);

/* Get and set minutes data
 * range: 00-59
 */
void ds1307_get_minutes (uint8_t * min);
void ds1307_set_minutes (uint8_t min);

/* Get and set hour data
 * range: 00-11 + AM/PM
 *     or 00-23
 * If the input value is greater than 12 but the RTC is set to AM/PM mode,
 * then the value will be mapped appropriatly;
 *        0-11 -> 0-11am, 12-23 -> 0-11pm
 */
#define DS1307_12HR 0x4
#define DS1307_PM   0x2
void ds1307_get_hours (uint8_t * hour, uint8_t * flags);
void ds1307_set_hours (uint8_t hour, uint8_t flags);

/* Get and set day of week
 * range: 01-07
 */
#define DS1307_SUN 1
#define DS1307_MON 2
#define DS1307_TUE 3
#define DS1307_WED 4
#define DS1307_THU 5
#define DS1307_FRI 6
#define DS1307_SAT 7
void ds1307_get_day (uint8_t * day);
void ds1307_set_day (uint8_t day);

/* Get and set day of the month
 * range: 00-30
 */
void ds1307_get_date (uint8_t * date);
void ds1307_set_date (uint8_t date);

/* Get and set month
 * range: 00-11
 */
void ds1307_get_month (uint8_t * month);
void ds1307_set_month (uint8_t month);

/* Get and set year offset.
 * Base year: 2000 (used to determine leap years and such, dont change)
 * range: 00-99
 * Current Year = 2000 + value
 */
#define DS1307_BASE_YEAR 2000
void ds1307_get_year (uint8_t * year);
void ds1307_set_year (uint8_t year);

#endif /* __CLOCK_DS1307 */
