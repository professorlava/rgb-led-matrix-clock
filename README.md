# Abstract LED Clock

Components Used:

* 1 x Atmel AVR ATmega328P  
* 1 x RGB LED Matrix, Common Cathode
* 1 x DS1307 RTC
* 8 x 74HC595 Shift Registers, SIPO

Peak current draw is 90.91mA
